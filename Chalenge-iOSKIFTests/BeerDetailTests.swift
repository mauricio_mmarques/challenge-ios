//
//  BeerDetailTests.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/2/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import Foundation
import KIF

class BeerDetailTests: BaseKIFTestCase {
    
    func testSeeBeerDetail() {
        tester().waitForView(withAccessibilityIdentifier: "BEER_CELL")
        tester().tapView(withAccessibilityIdentifier: "BEER_CELL")
        
        tester().waitForView(withAccessibilityIdentifier: "DETAIL_BEER_IMAGE_VIEW")
        tester().waitForView(withAccessibilityIdentifier: "DETAIL_BEER_NAME_LABEL")
        tester().waitForView(withAccessibilityIdentifier: "DETAIL_BEER_ABV_LABEL")
        tester().waitForView(withAccessibilityIdentifier: "DETAIL_BEER_IBU_LABEL")
        tester().waitForView(withAccessibilityIdentifier: "DETAIL_BEER_TAGLINE_LABEL")
        tester().waitForView(withAccessibilityIdentifier: "DETAIL_BEER_DESC_LABEL")
    }
    
}
