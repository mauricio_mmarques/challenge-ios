//
//  BaseKIFTestCase.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/2/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import XCTest
import UIKit
import KIF

class BaseKIFTestCase: KIFTestCase {
    
    override func beforeEach() {
        super.beforeEach()
        self.backToRoot()
    }
    
    func backToRoot() {
        if let rootViewController = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            rootViewController.popToRootViewController(animated: false)
        }
    }
    
}
