//
//  BeersTableViewDataSource.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/1/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import Foundation
import UIKit

class BeersTableViewDataSource: NSObject, UITableViewDataSource  {
    
    enum CellType {
        case beer
        case loading
        case undefined
    }
    
    var beers: [Beer]?
    
    fileprivate func getCellType(forRow row: Int) -> CellType {
        guard let beersCount = self.beers?.count, beersCount > 0 else {
            return .undefined
        }
        
        if row == beersCount {
            return .loading
        } else {
            return .beer
        }
    }
    
    //MARK: UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let beersCount = self.beers?.count, beersCount > 0 else {
            return 0
        }
        
        return beersCount + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellType = self.getCellType(forRow: indexPath.row)
        
        switch cellType {
        case .beer:
            if let beer = self.beers?[indexPath.row] {
                let cell = BeerTableViewCell(withBeer: beer)
                return cell
            }
        case .loading:
            let loadingCell = LoadingTableViewCell()
            return loadingCell
        case .undefined:
            break
        }
        
        return UITableViewCell()
    }
    
}
