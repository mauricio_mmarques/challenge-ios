//
//  Beer.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/1/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import Foundation
import ObjectMapper

struct Beer: Mappable {
    
    var name: String?
    var tagline: String?
    var description: String?
    var imageUrl: String?
    var abv: Double?
    var ibu: Int?
    
    init(name: String?, tagline: String?, description: String?, imageUrl: String?, abv: Double?, ibu: Int?) {
        self.name = name
        self.tagline = tagline
        self.description = description
        self.imageUrl = imageUrl
        self.abv = abv
        self.ibu = ibu
    }
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        self.name <- map["name"]
        self.tagline <- map["tagline"]
        self.description <- map["description"]
        self.imageUrl <- map["image_url"]
        self.abv <- map["abv"]
        self.ibu <- map["ibu"]
        
    }
    
}
