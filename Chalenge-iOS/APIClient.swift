//
//  APIClient.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/1/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class APIClient: Client {
    
    static let shared = APIClient()
    
    fileprivate let endpointManager = EndpointManager()
    
    fileprivate init() { }
    
    //MARK: Exposed API methods
    
    func getBeers(successCompletion: @escaping ([Beer]) -> (), errorCompletion: @escaping ErrorBlock) {
        self.getBeers(fromPage: nil, successCompletion: successCompletion, errorCompletion: errorCompletion)
    }
    
    func getBeers(fromPage page: Int?, successCompletion: @escaping SuccessBlock<[Beer]>, errorCompletion: @escaping ErrorBlock) {
        
        var parameters: [String:Any]?
        
        if let number = page {
            parameters = Parameters.page(number).keyAndValue
        }
        
        Alamofire.request(endpointManager.beersUrl, parameters: parameters, encoding: URLEncoding.default).responseJSON { (responseData) in
            self.responseHandler(responseData, successCompletion: { (beersJSON) in
                
                if let beersJSON = beersJSON as? [[String : Any]], let beers = Mapper<Beer>().mapArray(JSONArray: beersJSON) {
                    successCompletion(beers)
                } else {
                    errorCompletion(nil)
                }
                
            }, errorCompletion: errorCompletion)
        }
    }
    
    //MARK: Auxiliar methods
    
    fileprivate func responseHandler(_ responseData: DataResponse<Any>, successCompletion: @escaping SuccessBlock<Any>, errorCompletion: @escaping ErrorBlock) {
        if responseData.result.isSuccess {
            if let value = responseData.result.value {
                successCompletion(value)
            } else {
                errorCompletion(nil)
            }
        } else {
            errorCompletion(responseData.error)
        }
    }
    
}
