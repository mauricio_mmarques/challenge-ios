//
//  BeerTableViewCell.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/1/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import Kingfisher

let beerTableViewCellIdentifier = "beerTableViewCell"

class BeerTableViewCell: UITableViewCell, CodableView {
    
    let beerImageView = UIImageView()
    let beerNameLabel = UILabel()
    let beerAbvLabel = UILabel()
    
    let beer: Beer
    
    init(withBeer beer: Beer){
        self.beer = beer
        super.init(style: .default, reuseIdentifier: beerTableViewCellIdentifier)
        self.setupCodableView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: CodableView
    
    func buildViewHierarchy() {
        self.addSubview(self.beerImageView)
        self.addSubview(self.beerNameLabel)
        self.addSubview(self.beerAbvLabel)
    }
    
    func setupViewConstraints() {
        
        self.beerImageView.snp.makeConstraints { (make) in
            make.width.equalTo(30.0)
            make.height.equalTo(100.0)
            make.leading.equalToSuperview().offset(30.0)
            make.top.equalToSuperview().offset(15.0)
            make.bottom.equalToSuperview().offset(-15.0)
            
        }
        
        self.beerNameLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.beerImageView.snp.top)
            make.leading.equalTo(self.beerImageView.snp.trailing).offset(20.0)
            make.trailing.equalToSuperview().offset(-15.0)
        }
        
        self.beerAbvLabel.snp.makeConstraints { (make) in
            make.top.equalTo(self.beerNameLabel.snp.bottom).offset(10.0)
            make.leading.equalTo(self.beerNameLabel.snp.leading)
            make.trailing.equalToSuperview().offset(-15.0)
        }
        
    }
    
    func configureView() {
        self.contentView.accessibilityIdentifier = "BEER_CELL"
        self.beerNameLabel.text = self.beer.name
        self.beerNameLabel.numberOfLines = 0
        self.beerNameLabel.accessibilityIdentifier = "BEER_NAME_LABEL"
        
        if let abv = self.beer.abv {
            self.beerAbvLabel.text = String("Abv: \(abv)")
        } else {
            self.beerAbvLabel.text = "Abv: "
        }
        self.beerAbvLabel.accessibilityIdentifier = "BEER_ABV_LABEL"
        
        self.beerImageView.contentMode = .scaleAspectFit
        self.beerImageView.accessibilityIdentifier = "BEER_IMAGE_VIEW"
        if let imageStringUrl = self.beer.imageUrl, let imageUrl = URL(string: imageStringUrl) {
            self.beerImageView.kf.setImage(with: imageUrl, placeholder: UIImage(named: "beer_placeholder"))
        } else {
            self.beerImageView.image = UIImage(named: "beer_placeholder")
        }
    }
}
