//
//  Client.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/2/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import Foundation

typealias SuccessBlock<T> = (T) -> ()
typealias ErrorBlock = (Error?) -> ()

protocol Client {
    
    func getBeers(successCompletion: @escaping SuccessBlock<[Beer]>, errorCompletion: @escaping ErrorBlock)
    func getBeers(fromPage page: Int?, successCompletion: @escaping SuccessBlock<[Beer]>, errorCompletion: @escaping ErrorBlock)
    
}
