//
//  LoadingTableViewCell.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/1/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import UIKit

let loadingTableViewCellIdentifier = "loadingTableViewCell"

class LoadingTableViewCell: UITableViewCell {

    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    init() {
        super.init(style: .default, reuseIdentifier: loadingTableViewCellIdentifier)
        self.addSpinner()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func addSpinner() {
        self.addSubview(self.activityIndicator)
        
        self.accessibilityIdentifier = "LOADING_CELL"
        
        self.activityIndicator.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.centerY.equalToSuperview()
        }
    }
    
}
