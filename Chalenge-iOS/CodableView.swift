//
//  CodingView.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/1/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import Foundation

protocol CodableView {
    
    func configureView()
    func setupViewConstraints()
    func buildViewHierarchy()
    
    func setupCodableView()
    
}

extension CodableView {
    
    func setupCodableView() {
        self.buildViewHierarchy()
        self.setupViewConstraints()
        self.configureView()
    }
    
}
