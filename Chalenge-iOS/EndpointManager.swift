//
//  EndpointManager.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/1/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import Foundation

enum Parameters {
    case page(Int)
    
    var keyAndValue: [String:Any] {
        get {
            switch self {
            case .page(let pageNumber):
                return ["page":pageNumber]
            }
        }
    }
}

struct EndpointManager {
    
    let baseUrl = "https://api.punkapi.com/v2"
    
    var beersUrl: String {
        get {
            return self.baseUrl + "/beers"
        }
    }

}
