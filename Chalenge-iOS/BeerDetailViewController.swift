//
//  BeerDetailViewController.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/2/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import UIKit

class BeerDetailViewController: UIViewController, CodableView {
    
    let beer: Beer
    
    let scrollView = UIScrollView()
    let beerImageView = UIImageView()
    let beerNameLabel = UILabel()
    let beerAbvLabel = UILabel()
    let beerTaglineLabel = UILabel()
    let beerIbuLabel = UILabel()
    let beerDescriptionLabel = UILabel()
    
    init(withBeer beer: Beer) {
        self.beer = beer
        super.init(nibName: nil, bundle: nil)
        self.setupCodableView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func configureViewWithBeer() {
        if let imageStringUrl = self.beer.imageUrl, let imageUrl = URL(string: imageStringUrl) {
            self.beerImageView.kf.setImage(with: imageUrl, placeholder: UIImage(named: "beer_placeholder"))
        } else {
            self.beerImageView.image = UIImage(named: "beer_placeholder")
        }
        
        self.beerNameLabel.text = self.beer.name
        self.beerTaglineLabel.text = self.beer.tagline
        
        if let abv = self.beer.abv {
            self.beerAbvLabel.text?.append("\(abv)")
        }
        
        if let ibu = self.beer.ibu {
            self.beerIbuLabel.text?.append("\(ibu)")
        }
        
        self.beerDescriptionLabel.text = self.beer.description
    }
    
    //MARK: CodableView
    
    func buildViewHierarchy() {
        self.view.addSubview(self.scrollView)
        self.scrollView.addSubview(self.beerImageView)
        self.scrollView.addSubview(self.beerNameLabel)
        self.scrollView.addSubview(self.beerAbvLabel)
        self.scrollView.addSubview(self.beerTaglineLabel)
        self.scrollView.addSubview(self.beerIbuLabel)
        self.scrollView.addSubview(self.beerDescriptionLabel)
    }
    
    func setupViewConstraints() {
        
        self.scrollView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.beerImageView.snp.makeConstraints { (make) in
            make.width.equalTo(100.0)
            make.height.equalTo(250.0)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(20.0)
        }
        
        self.beerNameLabel.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(30.0)
            make.trailing.equalToSuperview().offset(-30.0)
            make.width.equalTo(260.0)
            make.top.equalTo(self.beerImageView.snp.bottom).offset(15.0)
        }
        
        self.beerTaglineLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(self.beerNameLabel.snp.leading)
            make.trailing.equalTo(self.beerNameLabel.snp.trailing)
            make.top.equalTo(self.beerNameLabel.snp.bottom).offset(5.0)
        }
        
        self.beerAbvLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(self.beerTaglineLabel.snp.leading)
            make.trailing.equalTo(self.beerTaglineLabel.snp.trailing)
            make.top.equalTo(self.beerTaglineLabel.snp.bottom).offset(15.0)
        }
        
        self.beerIbuLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(self.beerAbvLabel.snp.leading)
            make.trailing.equalTo(self.beerAbvLabel.snp.trailing)
            make.top.equalTo(self.beerAbvLabel.snp.bottom).offset(5.0)
        }
        
        self.beerDescriptionLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(self.beerIbuLabel.snp.leading)
            make.trailing.equalTo(self.beerIbuLabel.snp.trailing)
            make.top.equalTo(self.beerIbuLabel.snp.bottom).offset(18.0)
            make.bottom.equalToSuperview().offset(-50.0)
        }
        
    }
    
    func configureView() {
        self.title = self.beer.name
        self.scrollView.backgroundColor = .white
        
        self.beerImageView.contentMode = .scaleAspectFit
        self.beerImageView.accessibilityIdentifier = "DETAIL_BEER_IMAGE_VIEW"
        
        self.beerNameLabel.textAlignment = .center
        self.beerNameLabel.numberOfLines = 0
        self.beerNameLabel.font = UIFont.boldSystemFont(ofSize: 17.0)
        self.beerNameLabel.accessibilityIdentifier = "DETAIL_BEER_NAME_LABEL"
    
        self.beerAbvLabel.font = UIFont.systemFont(ofSize: 15.0)
        self.beerAbvLabel.text = "Abv: "
        self.beerAbvLabel.accessibilityIdentifier = "DETAIL_BEER_ABV_LABEL"
        
        self.beerIbuLabel.font = UIFont.systemFont(ofSize: 15.0)
        self.beerIbuLabel.text = "Ibu: "
        self.beerIbuLabel.accessibilityIdentifier = "DETAIL_BEER_IBU_LABEL"
        
        self.beerTaglineLabel.numberOfLines = 0
        self.beerTaglineLabel.textAlignment = .center
        self.beerTaglineLabel.font = UIFont.italicSystemFont(ofSize: 16.0)
        self.beerTaglineLabel.accessibilityIdentifier = "DETAIL_BEER_TAGLINE_LABEL"
        
        self.beerDescriptionLabel.font = UIFont.systemFont(ofSize: 16.0)
        self.beerDescriptionLabel.numberOfLines = 0
        self.beerDescriptionLabel.textAlignment = .justified
        self.beerDescriptionLabel.accessibilityIdentifier = "DETAIL_BEER_DESC_LABEL"
        
        self.configureViewWithBeer()
    }
    
}
