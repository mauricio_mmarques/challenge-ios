//
//  BeersListViewController.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/1/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import UIKit

class BeersListViewController: UIViewController, UITableViewDelegate {

    @IBOutlet weak var beersTableView: UITableView?
    
    var apiClient: Client {
        get {
            return APIClient.shared
        }
    }
    
    let beersTableViewDataSource = BeersTableViewDataSource()
    
    fileprivate var currentPage: Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupTableView()
        
        self.getBeers()
    }
    
    //MARK: TableView setup
    
    func setupTableView() {
        self.beersTableView?.dataSource = self.beersTableViewDataSource
        self.beersTableView?.delegate = self
        self.beersTableView?.register(BeerTableViewCell.self, forCellReuseIdentifier: beerTableViewCellIdentifier)
        self.beersTableView?.register(LoadingTableViewCell.self, forCellReuseIdentifier: loadingTableViewCellIdentifier)
        self.beersTableView?.rowHeight = UITableViewAutomaticDimension
        self.beersTableView?.estimatedRowHeight = 150.0
        self.beersTableView?.accessibilityIdentifier = "BEERS_TABLEVIEW"
    }
    
    //MARK: API methods
    
    func getBeers() {
        self.apiClient.getBeers(successCompletion: { beers in
            self.beersTableViewDataSource.beers = beers
            self.beersTableView?.reloadData()
        }) { (error) in
            self.presentAlert(withMessage: "An error occured when we tried to get your beers :(")
        }
    }
    
    func getMoreBeers() {
        self.apiClient.getBeers(fromPage: self.currentPage, successCompletion: { beers in
            self.beersTableViewDataSource.beers?.append(contentsOf: beers)
            self.beersTableView?.reloadData()
        }) { (error) in
            self.presentAlert(withMessage: "An error occured when we tried to get more your beers :(")
        }
    }
    
    //MARK: Alert method
    
    func presentAlert(withMessage message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let beer = self.beersTableViewDataSource.beers?[indexPath.row] {
            let detailBeerViewController = BeerDetailViewController(withBeer: beer)
            self.navigationController?.pushViewController(detailBeerViewController, animated: true)
        }
        
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if let loadingCell = cell as? LoadingTableViewCell {
            loadingCell.activityIndicator.startAnimating()
            self.currentPage += 1
            self.getMoreBeers()
        }

    }

}
