//
//  BeersListTests.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/2/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import XCTest
import KIF

class BeersListTests: BaseKIFTestCase {
        
    func testSeeBeersTableView() {
        tester().waitForView(withAccessibilityLabel: "Beers")
        tester().waitForView(withAccessibilityIdentifier: "BEERS_TABLEVIEW")
    }
    
    func testWaitToViewBeerCell() {
        tester().waitForView(withAccessibilityIdentifier: "BEER_CELL")
        tester().waitForView(withAccessibilityIdentifier: "BEER_NAME_LABEL")
        tester().waitForView(withAccessibilityIdentifier: "BEER_ABV_LABEL")
        tester().waitForView(withAccessibilityIdentifier: "BEER_IMAGE_VIEW")
    }
    
    func testTapOnBeerCell() {
        tester().tapView(withAccessibilityIdentifier: "BEER_CELL")
    }
    
    func testScrollLoadingMoreBeers() {
        tester().waitForView(withAccessibilityIdentifier: "LOADING_CELL")
    }

}
