//
//  EndpointManagerTests.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/2/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import XCTest
import Quick
import Nimble

@testable import Chalenge_iOS

class EndpointManagerTests: QuickSpec {
        
    override func spec() {
        
        let expectedBaseUrl = "https://api.punkapi.com/v2"
        let expectedBeersUrl = "https://api.punkapi.com/v2/beers"
        
        describe("The EndpointManager") {
            
            it("should have the baseUrl string equal to expectedBaseUrl") {
                expect(EndpointManager().baseUrl).to(equal(expectedBaseUrl))
            }
            
            it("should have the beersUrl string equal to expectedBeersUrl") {
                expect(EndpointManager().beersUrl).to(equal(expectedBeersUrl))
            }
            
        }
        
        describe("The Parameters enum") {
            
            it("should return the page key value") {
                let parameter = Parameters.page(1).keyAndValue
                let page = parameter["page"] as? Int
                expect(page).to(equal(1))
            }
            
        }
        
    }
    
}
