//
//  BeersListViewControllerTests.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/2/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import XCTest
import Quick
import Nimble
@testable import Chalenge_iOS

class BeersListViewControllerTests: QuickSpec {
    
    class MockAPIClient: Client {
        
        var beers: [Beer] = {
            var beers = [Beer]()
            for _ in 0..<5 {
                beers.append(Beer(name: "Beer", tagline: "The Best Beer", description: "The Best Beer in the world", imageUrl: "http://www.beers.com/beer.png", abv: 40.2, ibu: 13))
            }
            return beers
        }()
        
        var moreBeers: [Beer] = {
            var beers = [Beer]()
            for _ in 0..<3 {
                beers.append(Beer(name: "Beer", tagline: "The Best Beer", description: "The Best Beer in the world", imageUrl: "http://www.beers.com/beer.png", abv: 40.2, ibu: 13))
            }
            return beers
        }()

        
        var shouldReturnSuccess: Bool = true
        
        func getBeers(successCompletion: @escaping ([Beer]) -> (), errorCompletion: @escaping ErrorBlock) {
            self.getBeers(fromPage: nil, successCompletion: successCompletion, errorCompletion: errorCompletion)
        }
        
        func getBeers(fromPage page: Int?, successCompletion: @escaping ([Beer]) -> (), errorCompletion: @escaping ErrorBlock) {
            
            if self.shouldReturnSuccess {
                if page != nil {
                    successCompletion(self.moreBeers)
                } else {
                    successCompletion(self.beers)
                }
            } else {
                errorCompletion(nil)
            }
            
        }
        
    }
    
    class FakeBeersListViewController: BeersListViewController {
        
        let fakeTableView = UITableView()
        override var beersTableView: UITableView? {
            get {
                return fakeTableView
            }
            set {
                
            }
        }
        
        let mockApiClient = MockAPIClient()
        
        override var apiClient: Client {
            get {
                return mockApiClient
            }
        }
        
        var calledPresentAlertMethod = false
        var presentedAlertMessage = ""
        override func presentAlert(withMessage message: String) {
            self.calledPresentAlertMethod = true
            self.presentedAlertMessage = message
        }
        
    }
    
    override func spec() {
        
        describe("The BeersListViewController") {
            
            let beersListViewController = FakeBeersListViewController()
            
            _ = beersListViewController.view
            
            it("should setup fakeTableView") {
                if let beersTableView = beersListViewController.beersTableView {
                    expect(beersTableView.delegate).to(be(beersListViewController))
                    expect(beersTableView.dataSource).to(be(beersListViewController.beersTableViewDataSource))
                    expect(beersTableView.rowHeight).to(equal(UITableViewAutomaticDimension))
                    expect(beersTableView.estimatedRowHeight).to(equal(150.0))
                }
            }
            
            context("given success response from the APIClient") {
                it("should get initial beers list") {
                    expect(beersListViewController.beersTableViewDataSource.beers).toNot(beNil())
                    expect(beersListViewController.beersTableViewDataSource.beers?.count).to(equal(beersListViewController.mockApiClient.beers.count))
                }
                
                it("should load more beers and append in the existing beers list") {
                    beersListViewController.getMoreBeers()
                    
                    let allBeersCount = beersListViewController.mockApiClient.beers.count + beersListViewController.mockApiClient.moreBeers.count
                    
                    expect(beersListViewController.beersTableViewDataSource.beers).toNot(beNil())
                    expect(beersListViewController.beersTableViewDataSource.beers?.count).to(equal(allBeersCount))
                }
            }
            
            context("given error response from the APIClient") {
            
                beforeEach {
                    beersListViewController.calledPresentAlertMethod = false
                    beersListViewController.presentedAlertMessage = ""
                    
                    beersListViewController.mockApiClient.shouldReturnSuccess = false
                }
                
                it("should present get beers alert message") {
                    beersListViewController.getBeers()
                    expect(beersListViewController.calledPresentAlertMethod).to(beTruthy())
                    expect(beersListViewController.presentedAlertMessage).to(equal("An error occured when we tried to get your beers :("))
                }
                
                it("should present get beers alert message") {
                    beersListViewController.getMoreBeers()
                    expect(beersListViewController.calledPresentAlertMethod).to(beTruthy())
                    expect(beersListViewController.presentedAlertMessage).to(equal("An error occured when we tried to get more your beers :("))
                }
            }
            
        }
        
    }
    
}
