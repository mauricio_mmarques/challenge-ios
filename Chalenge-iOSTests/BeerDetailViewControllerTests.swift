//
//  BeerDetailViewControllerTests.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/2/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import XCTest
import Quick
import Nimble

@testable import Chalenge_iOS

class BeerDetailViewControllerTests: QuickSpec {
    
    override func spec() {
        
        describe("The BeerDetailViewController") {
            
            context("given a beer", { 
                
                let beer = Beer(name: "Beer", tagline: "The Best Beer", description: "The Best Beer in the world", imageUrl: "http://www.beers.com/beer.png", abv: 40.2, ibu: 13)
                
                let beerDetailViewController = BeerDetailViewController(withBeer: beer)
                
                _ = beerDetailViewController.view
                
                it("should set the beer's details in the labels") {
                    expect(beerDetailViewController.beerImageView.image).toNot(beNil())
                    
                    expect(beerDetailViewController.beerNameLabel.text).to(equal(beer.name))
                    expect(beerDetailViewController.beerTaglineLabel.text).to(equal(beer.tagline))
                    
                    if let abv = beer.abv {
                        expect(beerDetailViewController.beerAbvLabel.text).to(equal("Abv: \(abv)"))
                    } else {
                        expect(beerDetailViewController.beerAbvLabel.text).to(equal("Abv: "))
                    }
                    
                    if let ibu = beer.ibu {
                       expect(beerDetailViewController.beerIbuLabel.text).to(equal("Ibu: \(ibu)"))
                    } else {
                        expect(beerDetailViewController.beerIbuLabel.text).to(equal("Ibu: "))
                    }
                    
                    expect(beerDetailViewController.beerDescriptionLabel.text).to(equal(beer.description))
                }
                
            })
            
        }
        
    }
    
}
