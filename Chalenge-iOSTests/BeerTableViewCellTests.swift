//
//  BeerTableViewCellTests.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/2/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import XCTest
import Quick
import Nimble
@testable import Chalenge_iOS

class BeerTableViewCellTests: QuickSpec {
    
    override func spec() {
        
        describe("The BeerTableViewCell") {
            
            context("given a beer", {
                
                let beer = Beer(name: "Beer", tagline: "The Best Beer", description: "The Best Beer in the world", imageUrl: "http://www.beers.com/beer.png", abv: 40.2, ibu: 13)
                
                let beerTableViewCell = BeerTableViewCell(withBeer: beer)
                
                it("should set the beer's simple details in the labels") {
                    expect(beerTableViewCell.beerNameLabel.text).to(equal(beer.name))
                    
                    if let abv = beer.abv {
                        expect(beerTableViewCell.beerAbvLabel.text).to(equal("Abv: \(abv)"))
                    } else {
                        expect(beerTableViewCell.beerAbvLabel.text).to(equal("Abv: "))
                    }
                    
                    expect(beerTableViewCell.beerImageView.image).toNot(beNil())
            
                }
                
            })
            
        }
        
    }
    
}
