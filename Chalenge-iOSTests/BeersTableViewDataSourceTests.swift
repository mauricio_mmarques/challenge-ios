//
//  BeersTableViewDataSourceTests.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/2/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import XCTest
import Quick
import Nimble
@testable import Chalenge_iOS

class BeersTableViewDataSourceTests: QuickSpec {
    
    override func spec() {
        
        describe("The BeersTableViewDataSource") {
            
            context("given a list of beers", {
                
                let fakeTableView = UITableView()
                
                let beersTableViewDataSource = BeersTableViewDataSource()
                
                var beers = [Beer]()
                
                for _ in 0..<5 {
                    beers.append(Beer(name: "Beer", tagline: "The Best Beer", description: "The Best Beer in the world", imageUrl: "http://www.beers.com/beer.png", abv: 40.2, ibu: 13))
                }
                
                beforeEach {
                    beersTableViewDataSource.beers = beers
                }
                
                it("should return 1 tableView section") {
                    let expectedNumberOfSections = 1
                    expect(beersTableViewDataSource.numberOfSections(in: fakeTableView)).to(equal(expectedNumberOfSections))
                }
                
                it("should return beers.count + 1 for numberOfRows") {
                    let expectedNumberOfRows = beers.count + 1
                    expect(beersTableViewDataSource.tableView(fakeTableView, numberOfRowsInSection: 0)).to(equal(expectedNumberOfRows))
                }
               
                it("should return a BeerTableViewCell instance for cellForRow at indexPath 0") {
                    expect(beersTableViewDataSource.tableView(fakeTableView, cellForRowAt: IndexPath(row: 0, section: 0))).to(beAnInstanceOf(BeerTableViewCell.self))
                }
                
                it("should return a LoadingTableViewCell instance for cellForRow at the last indexPath") {
                    let lastIndexPath = beers.count
                    expect(beersTableViewDataSource.tableView(fakeTableView, cellForRowAt: IndexPath(row: lastIndexPath, section: 0))).to(beAnInstanceOf(LoadingTableViewCell.self))
                }
                
                it("should return a UITableViewCell instance for cellForRow with no beers list") {
                    beersTableViewDataSource.beers = [Beer]()
                    
                    expect(beersTableViewDataSource.tableView(fakeTableView, cellForRowAt: IndexPath(row: 0, section: 0))).to(beAnInstanceOf(UITableViewCell.self))
                }

            })
        }
    }
    
}
