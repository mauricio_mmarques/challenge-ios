//
//  LoadingTableViewCellTests.swift
//  Chalenge-iOS
//
//  Created by Maurício Marques on 7/2/17.
//  Copyright © 2017 Maurício Marques. All rights reserved.
//

import XCTest
import Quick
import Nimble
@testable import Chalenge_iOS

class LoadingTableViewCellTests: QuickSpec {
    
    override func spec() {
        
        describe("The LoadingTableViewCell") {
            
            context("when instanciated", {
                
                let loadingTableViewCell = LoadingTableViewCell()
                
                it("should containt an activity indicator view") {
                    
                    expect(loadingTableViewCell.activityIndicator.superview).to(equal(loadingTableViewCell))
                    
                }
                
            })
            
        }
        
    }
    
}
